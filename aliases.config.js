import path from "path";

export default {
  $lib: path.resolve("./src/lib"),
  $components: path.resolve("./src/lib/components"),
  $types: path.resolve("./src/lib/types"),
  $helpers: path.resolve("./src/lib/helpers"),
  $styles: path.resolve("./src/lib/styles"),
  $assets: path.resolve("./src/lib/assets"),
  $static: path.resolve("./static"),
};
